package com.scentlab.net.repository;

import com.scentlab.net.entity.Role;
import com.scentlab.net.models.ERole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByRoleName(ERole name);
    Role findByRoleId(int id);


}
