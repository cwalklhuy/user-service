package com.scentlab.net.repository;

import com.scentlab.net.entity.Account;
import com.scentlab.net.entity.AccountRole;
import com.scentlab.net.entity.AccountRolePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRoleRepository extends JpaRepository<AccountRole, AccountRolePK> {
    List<AccountRole> findByAccount(Optional<Account> DTO);
}
