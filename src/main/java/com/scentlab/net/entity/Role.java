package com.scentlab.net.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scentlab.net.models.ERole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "role")
public class Role {

    @Id
    @Column(name = "role_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer roleId;

    @Enumerated(EnumType.STRING)
    @Column(name = "role_name", nullable = false, length = 16)
    private ERole roleName;

//    @OneToMany(mappedBy = "role")
//    private Collection<AccountRole> accountRoles;

    @JsonIgnore
    @OneToMany(mappedBy = "role",cascade = {CascadeType.ALL})
    private Collection<AccountRole> accountRoles;
}
