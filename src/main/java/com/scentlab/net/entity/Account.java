package com.scentlab.net.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "account")
public class Account {

    @Id
    @Column(name = "username", length = 16)
    private String username;

    @Column(name = "password", length = 255, nullable = false)
    private String password;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "is_active")
    private Integer isActive;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private Integer phone;

    @JsonIgnore
    @OneToMany(mappedBy = "account",cascade = {CascadeType.MERGE})
    private Collection<AccountRole> accountRoles;

}
