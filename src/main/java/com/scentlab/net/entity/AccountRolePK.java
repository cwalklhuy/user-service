package com.scentlab.net.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class AccountRolePK implements Serializable {

    @Column
    private String username;
    @Column
    private Integer roleId;

    @Override
    public int hashCode()  {
        return Objects.hash(username, roleId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        AccountRolePK accountRolePK = (AccountRolePK) obj;
        return username.equals(accountRolePK.getUsername()) &&
                roleId.equals(accountRolePK.getRoleId());
    }
}
