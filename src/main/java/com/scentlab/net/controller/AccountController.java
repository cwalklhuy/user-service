package com.scentlab.net.controller;

import com.scentlab.net.payload.request.*;
import com.scentlab.net.payload.response.AccountJson;
import com.scentlab.net.payload.response.MessageResponse;
import com.scentlab.net.service.AccountService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/users")
public class AccountController {

    private final AccountService accountService;


    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(value = "/signin")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginReq loginRequest) {

        return accountService.login(loginRequest);
    }

    @PostMapping(value = "/signup-expert")
    public ResponseEntity<?> registerForExpert(@RequestBody SignupReq signupRequest) {

        return accountService.registerForExpert(signupRequest);
    }

    @PostMapping(value = "/signup-user")
    public ResponseEntity<?> registerUser(@RequestBody SignupReq signupRequest) {

        return accountService.registerUser(signupRequest);
    }

    @PostMapping(value = "/update-user", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> updateUser(@RequestBody UpdateAccountReq updateAccountReq) {

        return accountService.updateUser(updateAccountReq);
    }

    @GetMapping("/get-all")
    public List<AccountJson> getAll(){
        return accountService.getAll();
    }

    @PostMapping("/delete/{username}")
    public ResponseEntity<MessageResponse> deleteAccount(@PathVariable String username) {
         return accountService.deleteAccountByUsername(username);
    }


}
