package com.scentlab.net.service;

import com.scentlab.net.entity.Account;
import com.scentlab.net.entity.AccountRole;
import com.scentlab.net.entity.AccountRolePK;
import com.scentlab.net.entity.Role;
import com.scentlab.net.jwt.JwtUtils;
import com.scentlab.net.models.ERole;
import com.scentlab.net.payload.request.LoginReq;
import com.scentlab.net.payload.request.SignupReq;
import com.scentlab.net.payload.request.UpdateAccountReq;
import com.scentlab.net.payload.response.AccountJson;
import com.scentlab.net.payload.response.JwtResponse;
import com.scentlab.net.payload.response.MessageResponse;
import com.scentlab.net.repository.AccountRepository;
import com.scentlab.net.repository.AccountRoleRepository;
import com.scentlab.net.repository.RoleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AccountService {
    private final AuthenticationManager authenticationManager;

    private final JwtUtils jwtUtils;

    private final AccountRepository accountRepository;

    private final PasswordEncoder passwordEncoder;

    private final RoleRepository roleRepository;

    private final AccountRoleRepository accountRoleRepository;

    public AccountService(AuthenticationManager authenticationManager, JwtUtils jwtUtils, AccountRepository accountRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, AccountRoleRepository accountRoleRepository) {
        this.authenticationManager = authenticationManager;
        this.jwtUtils = jwtUtils;
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.accountRoleRepository = accountRoleRepository;
    }


    public ResponseEntity<?> login(LoginReq loginRequest) {
        final Authentication authenticate = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        final String jwt = jwtUtils.generateJwtToken(authenticate);

        UserDetailsImpl userDetails = (UserDetailsImpl) authenticate.getPrincipal();
        final List<String> roles = userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return ResponseEntity.ok(JwtResponse.builder()
                .token(jwt)
                .username(userDetails.getUsername())
                .roles(roles)
                .build());
    }


    public ResponseEntity<?> registerForExpert(SignupReq signupRequest) {

        String err = responseError(signupRequest);
        if (!err.isEmpty()) {

            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse(err));
        }


        final Account account = genAccount(signupRequest);

        final Set<String> strRoles = signupRequest.getRole();
        Set<Role> roles = new HashSet<>();
        if (strRoles == null) {
            final Role role = roleRepository.findByRoleName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found"));
            roles.add(role);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByRoleName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "expert":
                        Role modRole = roleRepository.findByRoleName(ERole.ROLE_EXPERT)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByRoleName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }
        final Account saveAccountToDB = accountRepository.save(account);
        roles.forEach(role -> {
            AccountRolePK accountRolePK = new AccountRolePK(saveAccountToDB.getUsername(), role.getRoleId());
            accountRoleRepository.save(new AccountRole(accountRolePK, account, role));
        });

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    public ResponseEntity<?> registerUser(SignupReq signupRequest) {
        String err = responseError(signupRequest);
        if (!err.isEmpty()) {

            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse(err));
        }
        final Account account = genAccount(signupRequest);

        Role userRole = roleRepository.findByRoleName(ERole.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));

        final Account saveAccountToDB = accountRepository.save(account);

        AccountRolePK accountRolePK = new AccountRolePK(saveAccountToDB.getUsername(), userRole.getRoleId());
        final Optional<AccountRole> accountRole = accountRoleRepository.findById(accountRolePK);
        if (!accountRole.isPresent()) {
            accountRoleRepository.save(new AccountRole(accountRolePK, account, userRole));
        }
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    private Account genAccount(SignupReq signupRequest) {
        return Account.builder()
                .username(signupRequest.getUsername())
                .password(passwordEncoder.encode(signupRequest.getPassword()))
                .isActive(1)
                .fullName(signupRequest.getFullName())
                .email(signupRequest.getEmail())
                .phone(signupRequest.getPhone())
                .build();
    }

    private String responseError(SignupReq signupRequest) {

        String result = "";
        if (accountRepository.existsByUsername(signupRequest.getUsername())) {
            result = "Error: Username is already taken!!!";
        }
        if (accountRepository.existsByEmail(signupRequest.getEmail())) {
            result = "Error: Email is already in use!!!";
        }

        return result;
    }

    public ResponseEntity<?> updateUser(UpdateAccountReq updateAccountReq) {

        final Optional<Account> account = accountRepository.findById(updateAccountReq.getUsername());

        if (account.isPresent()) {
            final Account accountDB = account.get();
            accountDB.setEmail(updateAccountReq.getEmail());
            accountDB.setFullName(updateAccountReq.getFullName());
            accountDB.setPhone(updateAccountReq.getPhone());
            accountRepository.save(accountDB);

            return ResponseEntity.ok(new MessageResponse("User updated successfully!"));
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new MessageResponse("Data not found"));
    }

    public List<AccountJson> getAll() {

        return accountRepository.findAllByIsActiveNot(0)
                .stream()
                .map(AccountJson::new)
                .collect(Collectors.toList());
    }

    public ResponseEntity<MessageResponse> deleteAccountByUsername(String username) {
        final Account account = accountRepository.findById(username)
                .orElseThrow(() -> new RuntimeException("Account not found"));
        account.setIsActive(0);
        accountRepository.save(account);
        return ResponseEntity.ok(new MessageResponse("Delete successfully!"));
     }
}
