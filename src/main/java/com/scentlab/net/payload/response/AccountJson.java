package com.scentlab.net.payload.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.scentlab.net.entity.Account;
import com.scentlab.net.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AccountJson {
    private String username;
    private String fullName;
    private Integer phone;
    private String email;

    public AccountJson(Account account) {
        this.username = account.getUsername();
        this.fullName = account.getFullName();
        this.phone = account.getPhone();
        this.email = account.getEmail();
    }
}
