package com.scentlab.net.payload.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SignupReq {
    private String username;
    private String password;
    private String fullName;
    private Integer phone;
    private String email;
    private Set<String> role;

}
