package com.scentlab.net.payload.request;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateAccountReq {
    private String username;
    private String fullName;
    private Integer phone;
    private String email;
}
